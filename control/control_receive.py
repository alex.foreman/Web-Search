import pika, json, datetime
import database.orm as db
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=db.engine)
orm = Session()

# TODO: Remove db stuff from file to database.db, add comments/documentation

def process_result(ch, method, properties, body):
    data = json.loads(body.decode('utf8'))
    for item in data['items']:
        orm.add(db.Results(Item_ID = item['Item_ID']
                          ,Alias_ID = item['Alias_ID']
                          ,Scan_ID = item['Scan_ID']
                          ,Event_ID = item['Event_ID']
                          ,Scanner_ID = item['Scanner_ID']
                          ,URL = item['URL']
                          ,Title = item['Name']
                          ,Date_Posted = None
                          ,Data = None
                          ,Date_Found = datetime.datetime.now()))
    orm.commit()

if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='127.0.0.1'))
    channel = connection.channel()
    channel.queue_declare(queue='results_queue')

    channel.basic_consume(process_result
                         ,queue='results_queue'
                         ,no_ack=True)

    print(' [*] Ready to receive results')
    channel.start_consuming()
