'''
This is the heart of the control engine.


If this is the main file, it creates and runs three threads.

First 
    The :func:`control.control.receive_thread` which handles the processing of 
    output from scanners into the database.
Second 
    The :func:`control.control.scan_thread` which waits around till a scan needs to run, 
    then starts up some scanners and fills their queues with items to scan for.
Third 
    The :func:`control.control.update_from_site` which waits for anything 
    in the "scan_change_queue" which is filled by the website when a change is made to any scans,
    when it finds one of these, it wakes the :func:`control.control.scan_thread` and tells it to recheck
    if it should be scanning.

TODO: Get rid of inline functions by using classes for threads.

TODO: --bash (scan events) Consider a testing database? This could also handle some input file.
TODO: --input_file

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>

'''
version = '0.00a'

import sys
sys.path.append('/vagrant')

import database.db as db
import database.orm as orm
from modules.functions import setup_logging
from modules import rabbitmq
import datetime, subprocess, json, uuid, threading, getopt, os, logging.config
from os import system
from time import sleep
from functools import partial

debug = False
testing = False
recheck = False

bash = False
input_file = False
push_queue = "TO Ebay:"

# orm.Scanner -> Bool, Exception
def deploy_scanners(scanner, out_queue, number_before_close):
    """
    Make sure there are scanners of the proper type running,
    if not start them up.

    TODO: make number_before_close an optional argument
    """

    #Sometimes, simply a path to a exe won't do like with a interpreted file
    #In this case, the "path" is actually a command

    if len(scanner.Path.split(' ')) != 1 :
        command = scanner.Path.split(' ')
    else:
        command = [scanner.Path]

    command += ['-q', out_queue, '-c', str(number_before_close)]
    if debug: 
        command += ['-d']
        logger.debug("Going to open scanner with: " + ' '.join(command))
    
    #Attempt to open, if can't open return error and put error in db
    #TODO: make this able to open onto other VMs and stuff (more dynamic)
    attempts = 0
    not_open = True
    while(attempts <= 30 and not_open):
        attempts += 1
        try:
            if scanner.Working_Directory is None:
                subprocess.Popen(command)
            else:
                subprocess.Popen(command, cwd=scanner.Working_Directory)
            not_open = False
        except Exception as err:
            logger.error(err)
            #print(command, scanner.Working_Directory)
            if attempts <= 30:
                sleep(30)
            else:
                db.make_error(
                    "control/deploy_scanners",
                    "30 attempts to run a scanner reached, error on last: " + err
                )
                return False, err
    return True, None


# Scan -> [string]
def get_items(scan):
    """
    Given a Scan object from the ORM, create a list of all item name strings
    and their aliases

    :param scan: A scan.
    :type scan: :class:`database.orm.Scan` 
    :rtype: List of Dictionaries {'Item_ID', 'Alias_ID', 'Product Name'}

    example::
    
        get_items(orm.query(db.Scan).one())
        >>> [{'Item_ID' : 2, 'Alias_ID' : 7, 'Product Name' : ['Medtronics', 'Medtronic']}
            ,{'Item_ID' : 3, 'Alias_ID' : None, 'Product Name' : ['HeartPump', 'HerdPump']}]
    

    """
    # First check if our scan is for all items or specific items
    # Based on that, create an item list string        
    if scan.All_Products == b'\x01':
        items = db.all_items()
    else:
        items = scan.Items
    item_dict = []
    for item in items:
        product_names = [item.Name]
        for alias in item.Alias:
            product_names += [alias.Alias]
        item_dict = item_dict + [
            {'Item_ID' : item.Item_ID
            ,'Alias_ID' : None
            ,'Product Name' : product_names}]
    return item_dict

# The main scan function
def do_scan():
    '''This is the funciton used by :func:`control.control.scan_thread` to
    actually run a scan.

    It takes no parameters because it re-runs :func:`database.db.next_scan` to fix
    any possibility that the thread loop missed a scan. If that happened, this
    would run the more previous scan, then get ReRun for the scan you intended.

    TODO: Better returns and error/traceback handling.

    :rtype: None or Error

    '''
    #Get all scans with Active=1 (True), and order them by the date of the next
    next_scan = db.next_scan()
    #If we are past the next scan, lets run that scan.
    if datetime.datetime.now() >= next_scan.Next_Date:
        logger.info("Doing This Scan: <name=%s> <id=%s>" % (next_scan.Name, next_scan.Scan_ID))

        # Sanity error checks
        #if len(next_scan.Scanners) < 1:
        #    raise NameError("ERROR: Scan %s has no scanners" % next_scan.Name)

        scan_event = db.make_event(next_scan)
        #Make sre the scan we want to run actually has some scanners it wants to use
        if len(next_scan.Scanners) > 0:
            # get_items(scan) returns a list of dicts with Item_ID, Alias_ID, Product Name
            #   this adds Scan_ID, Scanner_ID, Event_ID to all of those dicts.
            messages_relevent = {'Scan_ID'    : next_scan.Scan_ID
                                ,'Scanner_ID' : next_scan.Scanners[0].Scanner_ID
                                ,'Event_ID'   : scan_event.Event_ID}
            # Get items is a dict with Messages_relevent + these keys: 'Item_ID', 'Alias_ID', 'Product Name'.
            messages = list(map((lambda x: 
                json.dumps({**x, **messages_relevent})), get_items(next_scan)))
            
            #If bash=True don't create scanners or output info to rabbitMQ just print it all.
            if bash:
                for message in messages:
                    print(" [x] Sent data to RabbitMQ: \n")
                    print("       %s" % message)
            else:
                # setup rabbitMQ
                out_queue = "TO Ebay:"# + uuid.uuid4().urn[9:]
                #get a rabbitmq object and initialize it.
                rmq = rabbitmq.rabbit('control')
                out_queue = rmq.publish_queue
                # Send data to rabbitMQ channel
                for message in messages:
                    rmq.publish(message)
                    logger.debug(" [x] Sent data to RabbitMQ: ")
                    logger.debug("    %s" % message)
                # Start up scanners
                logger.debug("Attempting to open all <%s> scanners" % len(next_scan.Scanners))
                for scanner in next_scan.Scanners:
                    logger.debug("Scanner ID <%s>" % scanner.Scanner_ID)
                    scanner_deployed, er = deploy_scanners(
                        scanner, 
                        out_queue,
                        len(messages))
                    if er is not None:
                        logger.error(er)

                #close rabbitMQ connection
                rmq.close()


        # Add an End_Time to the scan event
        logger.info("Scan event_ID <%s>. All search terms sent to scanners." % scan_event.Event_ID)
        scan_event = db.end_event(scan_event)
        return None
    else:
        raise ValueError('control was started when it should not have been')

def scan_thread(cond):
    '''This is the scan thread.
    
    This function first funs :func:`database.db.next_scan` to see when the next scan is. 
    
    If the scan was in the past, it runs :func:`control.control.scan` otherwise 
    it waits until the next date of the next scan by calling :func:`threading.Condition.wait`
    on 30second intervals. 
    
    Can also be called to recheck :func:`database.db.next_scan` via :func:`threading.Condition.Notify`

    :param cond: condition used to wake and recheck.
    :param recheck: global used to know if we just timed out, or if we where worken so we need to recheck the scan database.
    :type cond: :func:`threading.Condition`
    :type recheck: Global Bool
    :rtype: None

    '''
    logger = logging.getLogger("Control.scan_thread")
    
    def run_at(run_at, interval=30):
        global recheck
        cond.acquire()
        while (not recheck) and datetime.datetime.utcnow() < run_at:
            if debug or bash: 
                if db.next_scan():
                    print("If '{0}' >= '{1}'".format(str(datetime.datetime.now()), str(db.next_scan().Next_Date)))
                else:
                    print("No next scan")
            cond.wait(interval)
            cond.acquire()
        recheck = False
        next_scan = db.next_scan()
        if next_scan:
            next_date = db.next_scan().Next_Date
            if datetime.datetime.utcnow() >= next_date:
                return True
            else:
                return False
        else:
            return False
    while True:
        nextscan = db.next_scan()
        if nextscan != False:
            if run_at(db.next_scan().Next_Date):
                do_scan()
        else:
            if run_at(datetime.datetime.utcnow() + datetime.timedelta(days=1)):
                do_scan()
    return "Closed"

def update_from_site(cond):
    '''Consume from scan_change_queue with internal function

    :param cond: lock condition used by scan_thread
    :param recheck: Global used by scan_thread to know if it was woken or timed out
    :type cond: :func:`threading.Condition` 
    :type recheck: Global Bool
    :rtype: None

    .. function:: scan_changed(ch, method, properties, body)

        This is the function that is called when a message is received
        in "scan_change_queue", it handles waking the scan_thread

        :param body: The actual body of the message is discarded, simply used to alert this thread.

    '''
    logger = logging.getLogger("Control.update_from_site")

    #this is bascially partial.
    def get_scan_changed_fn(scan_cond):
        def scan_changed(body):
            logger.info('Received update from site with message' + str(body) + ' Attempting to aquire scan_cond lock')
            global recheck
            scan_cond.acquire()
            logger.info('Acquired scan_cond lock')
            recheck = True
            scan_cond.notify()
            scan_cond.release()
        return scan_changed

    scan_change_fn = get_scan_changed_fn(cond)
    rabbitmq.consume_from_queue('site', scan_change_fn)


if __name__ == '__main__':
    #Handle startup arguments (mainly for test_suite)
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hvdt",
            ["help", "version", "debug", "testing", "bash", "push_queue=", "input_file="]
        )
    except getopt.GetoptError:
        print("Invalid options selected.")
        sys.exit()
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print("No help message at this time")
            sys.exit()
        elif opt in ('-v', '--version'):
            print(version)
            sys.exit()
        elif opt in ('-d', '--debug'):
            debug = True
        elif opt in ('-t', '--testing'):
            testing = True
        elif opt in ('--bash'):
            bash = True
        elif opt in ('--push_queue'):
            push_queue = arg
        elif opt in ('--input_file'):
            input_file = arg

    #Setup logging
    #TODO: Make run options effect this.
    if debug:
        print("LOGGER LEVEL DEBUG")
        setup_logging(default_level=logging.DEBUG)
    else:
        setup_logging(default_level=logging.INFO)

    logger = logging.getLogger('Control')

    logger.info('Control Engine is starting.')
    #Run control, Create threads.
        #receive_cond = threading.Condition()
        #receive = threading.Thread(target=receive_thread, args=(receive_cond,))
        #receive.start()

    try:
        logger.info('start scan thread.')
        scan_cond = threading.Condition()
        scan = threading.Thread(target=scan_thread, args=(scan_cond,))
        scan.start()

        '''Moving to ubuntu service for health.
        logger.info('start filter_results.py')
        subprocess.Popen(['python3', 'filter_results.py'])
        '''
        
        logger.info('start update_from_site thread.')
        update_from_site(scan_cond)
    except:
        raise