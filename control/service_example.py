# ---- Begin Service rabbitMQ example ---- #
import pika
import json

# Connect to a reabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(host="127.0.0.1"))

# Work with a specific channel on that server
channel = connection.channel()
channel.queue_declare(queue="search")

print(' [*] Waiting for messages. To exit press CTRL+C')

# Create a function to run when a message is received from rabbitMQ
# --Define this function to a specific situation in a channel.consume
def callback(ch, method, properties, body):
    print("Method: {}".format(method))
    print("Properties: {}".format(properties))

    #Data comes from rabbitMQ as bytes, but json requires string
    proper_body = body.decode('utf8').replace("'", '"')
    
    data = json.loads(proper_body)
    print("ID: {}".format(data['id']))
    print("Name: {}".format(data['name']))
    print("Description: {}".format(data['description']))

# Setup callback for run on search channel
channel.basic_consume(callback
                    , queue="search"
                    , no_ack=True)

# Wait for messages to enter rabbitMQ server+channel
channel.start_consuming()
# ---- End Service rabbitMQ example ---- #
