SERVERSIDE_TABLE_COLUMNS = [
    {
        "data_name": "A",
        "column_name": "Date",
        "default": "",
        "order": 1,
        "searchable": True
    },
    {
        "data_name": "B",
        "column_name": "Item Found",
        "default": "",
        "order": 2,
        "searchable": True
    },
    {
        "data_name": "C",
        "column_name": "Result Description",
        "default": "",
        "order": 3,
        "searchable": True
    },
    {
        "data_name": "D",
        "column_name": "Link",
        "default": "",
        "order": 4,
        "searchable": False
    },
    {
        "data_name": "E",
        "column_name": "Rating",
        "default": "",
        "order": 5,
        "searchable": False
    },
    {
        "data_name": "F",
        "column_name": "Result ID",
        "default": "",
        "order": 6,
        "searchable": False
    },
    {
        "data_name": "G",
        "column_name": "Item Type",
        "default": "",
        "order": 7,
        "searchable": False
    }
]
