#!/usr/bin/python
import sys
sys.path.append("/vagrant")
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/FlaskApp/")

from FlaskApp import app as application
application.secret_key = "Super secret Key!"