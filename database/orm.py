'''
SQLAlchemy ORM(Object Relation Model)


'''

# TODO: Update all documentation strings. There should be info about whats avalible and the types like lists
from sqlalchemy.ext.declarative import declarative_base, synonym_for
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Time, func, create_engine
from sqlalchemy.orm import relationship, backref, synonym, sessionmaker
import enum, datetime

engine = create_engine('mysql://master:NewAWSPassword1234@web-search.cmxgj4wqbqnn.us-west-2.rds.amazonaws.com:3306/webscanner?charset=utf8', encoding='utf-8', echo=False)
Base = declarative_base()

# Create mapping for the Scanner table.
class Scanner(Base):
    """
    This is the mapping for the Scanner table table
    """
    __tablename__ = "Scanner"

    Scanner_ID = Column(Integer, primary_key=True)
    Name = Column(String)
    Description = Column(String)
    Path = Column(String)
    Date_Added = Column(DateTime)
    Deleted = Column(Integer)
    Working_Directory = Column(String)

# Create the mapping for the Users table.
class Users(Base):
    """
    This is the mapping for the Users table table
    """
    __tablename__ = "Users"

    User_ID = Column(Integer, primary_key=True)
    First_Name = Column(String, nullable=False)
    Middle_Name = Column(String)
    Last_Name = Column(String)
    Password = Column(String)

    def __repr__(self):
        return "<User(First_Name='%s', Middle_Name='%s', Last_Name='%s', Password='%s')>" % (self.First_Name, self.Middle_Name, self.Last_Name, self.Password)

# Create mapping for the Notification_Methods table.
#   First create the enum for notification_type
''' Not working, because I can't have a - in variable name
TODO: Consider Fixing.
class Notification_Types(enum.Enum):
    phone = 1
    e-mail = 2
    text = 3
'''
class Notification_Methods(Base):
    """
    This is the mapping for the Notification_Methods table table
    """
    __tablename__ = "Notification_Methods"

    Method_ID = Column(Integer, primary_key=True)
    User_ID = Column(Integer, ForeignKey('Users.User_ID'))
    #Notification_Type = Column(Enum(Notification_Types), nullable=False)
    Notification_Type = Column(String, nullable=False)
    Notification_Value = Column(String, nullable=False)

    User = relationship("Users", backref=backref('Notification_Methods', uselist=True))

    def __repr__(self):
        return "<Notification_Methods(Notification_Type='%s', Notification_Value='%s')>" % (self.Notification_Type, self.Notification_Value)

# Create mapping for the Scan_Notification table.
class Scan_Notification(Base):
    """
    This is the mapping for the Scan_Notification table table
    """
    __tablename__ = "Scan_Notification"

    Scan_Notification_ID = Column(Integer, primary_key=True)
    Scan_ID = Column(Integer) 
    Method_ID = Column(Integer)

    def __repr__(self):
        return "<Scan_Notification(Scan_ID='%s', Method_ID='%s')>" % (self.Scan_ID, self.Method_ID)

# Create mapping for the Scan table.
class Scan(Base):
    """
    This is the mapping for the Scan database table
    """
    __tablename__ = "Scan"

    Scan_ID = Column(Integer, primary_key=True)
    Name = Column("Scan_Name", String)
    Start_Date = Column(DateTime, default=func.now())
    End_Date = Column(DateTime, nullable=True)
    Next_Date = Column(DateTime, nullable=True)
    Repeat = Column("Scan_Repeat", Integer, nullable=False)
    Active = Column(Integer, nullable=False, default=1)
    Description = Column(String)
    All_Products = Column(Integer, nullable=False, default=1)
    Gap_Days = Column(Integer)
    Gap_Time = Column(Time(timezone=False), nullable=False, default=datetime.time(0, 0, 0))
    #Get an array of scanners, using the scan_scanner table
    Scanners = relationship("Scanner", secondary='Scan_Scanner', uselist=True)
    Items = relationship("Item", secondary='Scan_Items', uselist=True)


# Create mapping for the Scan_Items relationship table.
class scan_items(Base):
    """
    This is the mapping for the Scan_Items database table
    """
    __tablename__ = "Scan_Items"

    #Scan_Items_ID = Column(Integer, primary_key=True)
    Scan_ID = Column(Integer, ForeignKey('Scan.Scan_ID'), primary_key=True)
    Item_ID = Column(Integer, ForeignKey('Item.Item_ID'), primary_key=True)

# Create mapping for the Scan_Scanner relationship table.
class Scan_Scanner_Link(Base):
    """
    This is the mapping for the Scan_Scanner table, it is used to map scanners onto scan
    """
    __tablename__ = "Scan_Scanner"

    Scan_ID = Column(Integer, ForeignKey('Scan.Scan_ID'), primary_key=True)
    Scanner_ID = Column(Integer, ForeignKey('Scanner.Scanner_ID'), primary_key=True)


# Create mapping for the Scan_Event table.
class Scan_Event(Base):
    """
    This is the mapping for the Scan_Event table table
    """
    __tablename__ = "Scan_Event"

    Event_ID = Column(Integer, primary_key=True)
    Scan_ID = Column(Integer, ForeignKey('Scan.Scan_ID'))
    Start_Time = Column(DateTime)
    End_Time = Column(DateTime)


# Create mapping for the Item table.
class Item(Base):
    """
    This is the mapping for the Item table table
    """
    __tablename__ = "Item"

    Item_ID = Column(Integer, primary_key=True)
    Name = Column("Item_Name", String)
    Type = Column(String)
    Date_Added = Column(DateTime)
    Deleted = Column(Integer)


# Create mapping for the Item_Alias relationship table.
class Item_Alias(Base):
    """
    This is the mapping for the Item_Alias table table
    """
    __tablename__ = "Item_Alias"

    Alias_ID = Column(Integer, primary_key=True)
    Item_ID = Column(Integer, ForeignKey('Item'))
    Alias = Column(String)
    Alias_Deleted = Column(Integer)

    Item = relationship("Item", backref=backref('Alias', uselist=True))


# Create mapping for the Results table.
class Results(Base):
    """
    This is the mapping for the Results table table
    """
    __tablename__ = "Results"

    Result_ID = Column(Integer, primary_key=True)
    Item_ID = Column(Integer, ForeignKey('Item'))
    Alias_ID = Column(Integer, ForeignKey('Item_Alias'))
    Scan_ID = Column(Integer, ForeignKey('Scan'))
    Event_ID = Column(Integer, ForeignKey('Scan_Event'))
    Scanner_ID = Column(Integer, ForeignKey('Scanner'))
    URL = Column(String)
    Title = Column(String)
    Date_Posted = Column(DateTime)
    Data = Column(String)
    Date_Found = Column(DateTime)

    Item = relationship("Item", backref="Results")
    Alias = relationship("Item_Alias", backref="Results")
    Scan = relationship("Scan", backref="Results")
    Event = relationship("Scan_Event", backref="Results")
    Scanner = relationship("Scanner", backref="Results")

# Create mapping for the settings table
class Settings(Base):
    """
    This is the mapping for the Settings table
    """
    __tablename__ = "Settings"
    id = Column(Integer, primary_key=True)
    Setting = Column(String)
    Value = Column(String)

# Create mapping for the Errors table
class Errors(Base):
    """
    This is the mapping for the Errors table
    """
    __tablename__ = "Errors"

    id = Column(Integer, primary_key=True)
    Thrower = Column(String, nullable=True)
    When = Column(DateTime, default=func.now(), nullable=True)
    Error = Column(String, nullable=True)

'''
# Create mapping for the Scanner_Options relationship table.
class Scanner_Options(Base):
    """
    This is the mapping for the Scanner_Options table table
    """
    __tablename__ = "Scanner_Options"

    Option_ID = Column(Integer, primary_key=True)
    Scanner_ID = Column()
    Type = Column()
    Name = Column()
    Default = Column()

# Create mapping for the Scan_Scanner_Options table.
class Scan_Scanner_Options(Base):
    """
    This is the mapping for the Scan_Scanner_Options table table
    """
    __tablename__ = "Scan_Scanner_Options"

    Scan_Scan_Op_ID = Column(Integer, primary_key=True)
    Scan_Scanner_ID = Column()
    Option_ID = Column()
    Value = Column()
'''

def get_orm_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session
