START TRANSACTION;
INSERT INTO Item (`Name`, Date_Added, Deleted)
VALUES ('HeartPump', '2017-07-28 10:17:19', 0),
('Stent', '2017-10-19 00:18:00', 1);

INSERT INTO Item_Alias (Item_ID, Alias)
VALUES (1, 'hertpump'),
(1, 'HeartPump'),
(2, 'Stent');

INSERT INTO Notification_Methods (User_ID, Notification_Type, Notification_Values)
VALUES (1, 'phone', '9515297601'),
(1, 'e-mail', 'scott.r.meyer@gmail.com');

INSERT INTO Results (Item_ID, Alias_ID, Scan_ID, Event_ID, Scanner_ID, `URL`, Title, Date_Posted, `Data`)
VALUES (1, 2, 1, 1, 1, 'http://ebay.com/8s70dd72', 'Get this amazing Heart Pump!', '2011-08-08 10:03:18', NULL);

INSERT INTO Scan (`Scan_Name`, `Start_Date`, End_Date, Next_date, `Scan_Repeat`, Active, `Description`, All_Products, Gap_Days, Gap_Time)
VALUES ('first', '2017-11-01 10:12:00', '2017-12-05 00:00:00', '2017-11-03 10:12:00', 1, 1, 'This is some test data', 1, 2, '00:00:00');

INSERT INTO Scan_Event (Scan_ID, Start_Time, End_Time)
VALUES (1, '2017-11-01 10:13:00', '2017-11-01 14:18:22');

INSERT INTO Scan_Items (Scan_ID, Item_ID)
VALUES (1, 1);

INSERT INTO Scan_Notification (Scan_ID, Method_ID)
VALUES (1, 2);

INSERT INTO Scan_Scanner (Scan_ID, Scanner_ID)
VALUES (1,1);

INSERT INTO Scan_Scanner_Options (Scan_Scanner_ID, Option_ID, `Value`)
VALUES (1, 1, 0);

INSERT INTO Scanner (`Name`, `Description`, `Path`, Date_Added, Deleted)
VALUES ('Ebay', 'This searches ebay', './ebay.py', '2017-10-31 08:27:11', 0),
('Ebay', 'This is the old eBay', '/usr/tmp/ebay.py', '2017-10-20 18:28:42', 1);

INSERT INTO Scanner_Options (Scanner_ID, `Type`, `Name`, `Default`)
VALUES (1, 'bool', 'Search Closed', 0),
(1, 'text', 'UserName', NULL);

INSERT INTO Users (First_Name, Middle_Name, Last_Name, `Password`)
VALUES ('Scott', NULL, 'Meyer', 'hello');
COMMIT;