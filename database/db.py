'''
The database.db module contains some useful functions for utilising the :mod:`database.orm`

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>

'''

# TODO: add comments and documentation here.

import sys
sys.path.append('/vagrant')

import database.orm as orm
import datetime, logging
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=orm.engine)
session = Session()
logger = logging.getLogger("db")

    # get the next scan
def next_scan():
    '''Find the next scan in the schedule
    
    :rtype: Instance of :class:`database.orm.Scan`

    '''
    def valid_scan(x):
        if x.Next_Date is not None:
            if x.End_Date is not None:
                return x.End_Date >= datetime.datetime.utcnow()
            else: return True
        else: return False
    session.commit()
    scans = list(filter(valid_scan
        , session.query(orm.Scan).filter(orm.Scan.Active==1).order_by(orm.Scan.Next_Date).all()))
    if len(scans) <= 0:
        logger.info("Didn't find a next scan.")
        return False
    else:
        return scans[0]

def make_event(scan):
    '''Make and commit a :class:`database.orm.Scan_Event` to the database

    :param scan: the scan that needs a new event
    :type scan: :class:`database.orm.Scan`
    :rtype: :class:`database.orm.Scan_Event`

    '''
    # First, add the event of this scan to the database
    scan_event = orm.Scan_Event(Scan_ID = scan.Scan_ID
                           ,Start_Time = datetime.datetime.now()
                           ,End_Time = None)
    session.add(scan_event)
    if scan.Repeat == b'\x00':
        scan.Active = b'\x00'
    # Because adding an event has a SQL trigger, we need to commit
    session.commit()
    return scan_event

def end_event(event):
    '''Scan events are made with a blank End_Time, fill end time with now and commit

    :param event: The event that neds a end_time
    :type event: :class:`database.orm.Scan_Event`
    :rtype: :class:`database.orm.Scan_Event`

    '''
    event.End_Date = datetime.datetime.now()
    session.commit()
    return event

def all_items():
    '''All non-deleted items in the database

    :rtype: list of :class:`database.orm.Item`

    '''
    return session.query(orm.Item).filter_by(Deleted=0).all()

def make_error(who, what):
    '''
    Make an error entry into the Errors table of the databse.

    :param who: this is the Thrower part, where is this error happening
    :param what: This is the "Error" the actual error message body

    '''
    error = orm.Error(
        Thrower = who,
        When = datetime.datetime.now(),
        Error = what
    )
    logger.info("Adding error to database.")
    session.add(error)
    session.commit()