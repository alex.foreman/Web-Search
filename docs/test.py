#The follow function squares input
def squareThis(y):
    """This function is used to get the square of a variable
     >>>squareThis(10)
    100
    """
    return y*y
