Filter Service
==============

* :ref:`filter`

The Filtering Service accepts JSON data directly from Scan Services. They must accept JSON data of the form:

.. code-block:: javascript

    {
        'Scanner_ID' : 123,
        'Event_ID' : 123,
        'Scan_ID' : 123,
        'Item_ID' : 123,
        'Alias_ID' : None,
        'Product Name' : ['name1', 'name2'],
        'Items' :
            [
                {
                    'Name' : 'Title of listing',
                    'URL' : 'Product URL',
                    'Category' : 'Primary category from e-bay listing'
                },
                {
                    'Name' : 'Title of listing',
                    'URL' : 'Product URL',
                    'Category' : 'Primary category from e-bay listing'
                }
            ]
    }

As each result is received, the Filtering Service attempts to decide whether or not the result is a good result based on
what was being searched for. This can be changed within the filtering script.


.. _filter:

Filter Script
-------------
.. automodule:: control.filter_results
   :members:
   :undoc-members: