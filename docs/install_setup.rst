
Installation and Setup
**********************

First choose your deployment style. 

At current you can:

* :ref:`Local_Installation` create a local install of the webscanner
* :ref:`AWS_Deployment` create a ubuntu AWS VM to deoply your scanner to the cloud.

.. _Local_Installation:

Local Installation
==================

System Preperation
------------------

Due to the complex enviorment required for all of the different pieces of this project to interact, we use a lightweight virtual machine to run the project.

Our virtual machine managment system of choice was Vagrant_.

Vagrant requires a Virtual Machine engine to work through, VirtualBox_ is recomended.

You will also need to download this_ project from gitlab.

.. _Vagrant: https://www.vagrantup.com/
.. _VirtualBox: https://www.virtualbox.org/
.. _this: https://gitlab.com/flyingpenguin900/Web-Search/

Getting Started
---------------

Once you have your system prepared to run the project, open a CommandPromp/Terminal window and browse to folder that contains the webscanner project, then enter ``vagrant up``

Windows CommandPrompt::

    >cd C:\Users\Documents\GitHub\Web-Search
    >vagrant up

Linux/Mac Terminal::

    >cd GitHub/Web-Search
    >vagrant up

Wait for the setup process to finish (This make take a while). Once complete, open a browser on your local machine and browse to http://127.0.0.1:5678 to see the project.

WARNING
--------

At current, all deployments of the Web Scanner project are hardcoded to use the same AWS cloud database. 

This means, that if you start your own local deployment which includes the "Control Engine" and another deployment that contains the "Control Engine" is running, there may be complcations. To turn off your control engine follow these steps::

    >vagrant ssh
    >sudo systemctl stop websearch-control

.. _AWS_Deployment:

AWS Deployment
==============

.. _`this file`: https://gitlab.com/flyingpenguin900/Web-Search/blob/master/amazon_userdata

Deployment is made somewhat easier by the inclusion of a AWS deployment script in the project.

Create an AWS account, then go to the EC2 instaces and create a new Ubuntu instance

While creating the instance, be sure to "Edit User Data" and copy and paste the code from `this file`_ into the user data field.

Thats it, deploy the instance and once it has finished setting up, you are good to go. Simply browse to the instance on your local bwoser at port **5000**