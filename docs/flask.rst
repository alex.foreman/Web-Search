FlaskApp/Webserver
==================


Flask consists of app.py which you run, and db.py which it imports

* :ref:`app`
* :ref:`flask_db`

.. _app:

Flask APP
--------------
.. automodule:: FlaskApp.FlaskApp.__init__
   :members: 
   :undoc-members:

.. _flask_db:

FlaskApp.db
-----------
.. automodule:: FlaskApp.FlaskApp.db
   :members:
   :undoc-members:
