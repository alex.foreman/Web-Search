General Use Modules
====================

These are general use modules to facilitate interaction with the more 
external parts of Web Scanner.


* :ref:`database_db`
    For interacting with the database.
* :ref:`modules_rabbitmq`
    For interacting with any other modules of the code.
* :ref:`database_orm`
    How interaction with the database should be handled if you need to do it manually.

.. _database_db:

database.db
-----------
.. automodule:: database.db
   :members: 
   :undoc-members:

.. _modules_rabbitmq:

modules.rabbitmq
------------------
.. automodule:: modules.rabbitmq
   :members:
   :undoc-members:

.. _database_orm:

database.orm
------------
.. automodule:: database.orm
   :members: 
   :undoc-members: