Code Architecture
==============================


* :ref:`main_code`
* :ref:`database`
* Find more explanation in the following presentations:
    * Technologies: https://prezi.com/p/rvwb4zlprpy1/
    * Design Destions: https://prezi.com/p/v1dtdo5tddrw/

.. _main_code:

System Design
--------------

.. figure:: img/Dataflow.png

    Flow of data and design

.. _database:

Database Design
----------------

.. figure:: img/database.png

    Database design